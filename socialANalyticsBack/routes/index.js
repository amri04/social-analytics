var express = require('express');
var router = express.Router();
const { exec } = require('child_process');
/* GET home page. */
router.post('/:filename', function(req, res, next) {
  let filename = req.params.filename;
  console.log(filename);
  
  exec('python pyScript.py '+ filename, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
  });
});

module.exports = router;
