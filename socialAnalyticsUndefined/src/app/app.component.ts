import { Component } from '@angular/core';
import {Http} from "@angular/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'socialAnalyticsUndefined';
  imageName="output.jpg";
  serverUrl="http://127.0.0.1:3000/"

  constructor(private http: Http) {
  }

  postDataSetName(fileInput){
    let file = fileInput.target.files[0];
    let fileName = file.name;
    console.log(fileName);
    
    this.http.get(this.serverUrl+fileName).subscribe(res=>{
      console.log(res);
      
    })
  }
}
